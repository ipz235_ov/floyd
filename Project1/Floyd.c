#include <stdio.h>
#include <windows.h>

#define INF 99999
#define V 4 // ʳ������ ������ �����

// ������� ��� ��������� ������� ��������
void printSolution(int dist[][V]) {
    printf("������� ��������:\n");
    for (int i = 0; i < V; i++) {
        for (int j = 0; j < V; j++) {
            if (dist[i][j] == INF)
                printf("%7s", "INF");
            else
                printf("%7d", dist[i][j]);
        }
        printf("\n");
    }
}

// �������, ��� ������ �������� ������
void floydWarshall(int graph[][V]) {
    int dist[V][V];
    // ������������ ������� �������� �� ��������� �����
    for (int i = 0; i < V; i++)
        for (int j = 0; j < V; j++)
            dist[i][j] = graph[i][j];

    // ������������ ��������� ������
    for (int k = 0; k < V; k++) {
        for (int i = 0; i < V; i++) {
            for (int j = 0; j < V; j++) {
                // ��������� ������� ���� � �������� ����
                if (dist[i][k] + dist[k][j] < dist[i][j])
                    dist[i][j] = dist[i][k] + dist[k][j];
            }
        }
    }

    // ��������� ����������
    printSolution(dist);
}

int main() {
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    int graph[V][V] = { {0, 5, INF, 10},
                        {INF, 0, 3, INF},
                        {INF, INF, 0, 1},
                        {INF, INF, INF, 0} };
    floydWarshall(graph);
    return 0;
}
